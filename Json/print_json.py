import ruamel.yaml
import sys

result = ''

yml = ruamel.yaml.YAML()
yml.indent(mapping=2, sequence=4, offset=2)

print(ruamel.yaml.dump(result, Dumper=ruamel.yaml.RoundTripDumper, indent=4))
