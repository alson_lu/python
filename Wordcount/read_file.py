import webbrowser
import requests


filepath = "../file/ItemsToImport.txt"

with open(filepath) as fp:
    line = fp.readline()
    cnt = 1
    itemString = ''
    while line:
        line = fp.readline()
        cnt += 1
        if cnt < 100:
            itemString = itemString + line.rstrip()
        elif cnt >= 100:
            cnt = 0
            print 'https://qa/soa/inventory-3/2/item/index?type=ATTRIBUTE&indexMode=sync&anchors=' + itemString[:-1]
            itemString = ''
            print ''
    print 'https://qa/soa/inventory-3/2/item/index?type=ATTRIBUTE&indexMode=sync&anchors=' + itemString[:-1]